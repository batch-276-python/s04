# Python Object-Oriented Programming
[]: # with this snippet from the same file:
[]: # # Python Classes and Objects

### Importing abc method for abstract classes
``` python
from abc import ABC, abstractmethod
```

### Inheriting from parent class
``` python
class <ClassName>(<ParentClassName>):
    def __init__(self):
        super().__init__()
```