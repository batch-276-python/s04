# S04 Activity
from abc import ABC, abstractmethod


# 1. Create an abstract class called Animal that has the following abstract methods:
# eat(food)
# make_sound()


class Animal(ABC):

    @abstractmethod
    def eat(self, food):
        pass

    @abstractmethod
    def make_sound(self):
        pass


# 2. Create two classes that implements the Animal class called Cat and Dog
# With each of the following properties:
# Name
# Breed
# Age
# And the following methods:
# Getters
# Setters
# Implementation of abstract methods
# call()

class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()

        self.name = name
        self.breed = breed
        self.age = age

    def get_name(self):
        print (f"Name of cat: {self.name}")

    def set_name(self, name):
        self.name = name

    def get_breed(self):
        print (f"Breed of cat: {self.breed}")

    def set_breed(self, breed):
        self.breed = breed

    def get_age(self):
        print (f"Age of cat: {self.age}")

    def eat(self, food):
        print(f"{self.name} is eating {food}")

    def make_sound(self):
        print(f"{self.name} is making sound. Meow!")

    def call(self):
        print(f"{self.name} is calling. Moshi! Moshi!")


class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()

        self.name = name
        self.breed = breed
        self.age = age

    def get_name(self):
        print(f"Name of dog: {self.name}")

    def set_name(self, name):
        self.name = name

    def get_breed(self):
        print(f"Breed of dog: {self.breed}")

    def set_breed(self, breed):
        self.breed = breed

    def get_age(self):
        print(f"Age of dog: {self.age}")

    def set_age(self, age):
        self.age = age

    def eat(self, food):
        print(f"{self.name} is eating {food}")

    def make_sound(self):
        print(f"{self.name} is making sound. Woof!")

    def call(self):
        print(f"{self.name} is calling. Gau Gau!")


# Test Cases:

dog1 = Dog("Chessy", "Siberian Husky", 2)
dog1.get_name()
dog1.get_breed()
dog1.get_age()
dog1.eat("meat")
dog1.make_sound()
dog1.call()

cat1 = Cat("Mimi", "Persian", 1)
cat1.get_name()
cat1.get_breed()
cat1.get_age()
cat1.eat("fish")
cat1.make_sound()
cat1.call()

