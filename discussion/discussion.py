from abc import ABC
# this import tells the program to get the abc module of python to be used in the program
# ABC is a class that is used to create abstract classes


# use () on className to verify if ti will use inheritance
class SampleClass:

    def __init__(self, year):
        self.year = year

    def show_year(self):
        print(f"The year is {self.year}")


sample_obj = SampleClass(2023)

print(sample_obj.year)
sample_obj.show_year()

# [Section]v Fundamentals of OOP
# Four Pillars of OOP
# 1. Encapsulation
# 2. Abstraction
# 3. Inheritance
# 4. Polymorphism

# First Pillar: Encapsulation
# Encapsulation is a mechanism of wrapping the attributes and code acting on the methods together as a single unit
# use _ to make a variable private (sample_obj._name)


class Person:
    def __init__(self, name, age):
        self._name = name
        self._age = age

    # Methods
    # getter of _name attribute
    def get_name(self):
        print(f"Name of person: {self._name}")

    # setter of _name attribute
    def set_name(self, name):
        self._name = name

    # getter of _age attribute
    def get_age(self):
        print(f"Age of person: {self._age}")

    # setter of _age attribute
    def set_age(self, age):
        self._age = age


# new instance of Person class
person_one = Person("Ian", 27)

# print(person_one._name) # accessing the _name attribute. This is not recommended. Use the getter method instead

person_one.get_name()

# setting the _name attribute using the setter method
person_one.set_name("Christoper")
person_one.get_name()

person_one.get_age()

# setting the _age attribute using the setter method
person_one.set_age(28)
person_one.get_age()


# Second Pillar: Inheritance
# Inheritance is a mechanism of using details from a new class without modifying existing class
# The inheritance of the characteristics or attributes of a parent class to a child class is called inheritance

# Syntax: class ChildClassName(ParentClassName): pass # pass is a keyword that does nothing but is required by python

class Employee(Person):
    def __init__(self, name, age, employee_id):
        # super() can be used to invoke immediate parent class constructor
        # in this case, it will invoke the constructor of the Person class. Similar to calling Person.__init__()
        super().__init__(name, age)

        self._employee_id = employee_id

    # getter of _employee_id attribute
    def get_employee_id(self):
        print(f"The Employee ID is {self._employee_id}")

    # setter of _employee_id attribute
    def set_employee_id(self, employee_id):
        self._employee_id = employee_id


# new instance of Employee class
employee_one = Employee("CongTV", 27, "00001")

employee_one.get_name()
employee_one.get_age()
employee_one.get_employee_id()

employee_one.set_age(30)
employee_one.get_age()

employee_one.set_employee_id("00002")
employee_one.get_employee_id()


# Third Pillar: Polymorphism
# Polymorphism is a mechanism of using a single type entity (method, operator or object)
# to represent different types in different scenarios
# the same method name can be used for different classes

# Example of Polymorphism

class TeamLead:
    def occupation(self):
        print("Team Lead")

    def has_auth(self):
        print(True)


class TeamMember:
    def occupation(self):
        print("Team Member")

    def has_auth(self):
        print(False)


team_lead = TeamLead()
team_member = TeamMember()

# Use For Loop to check if the class has the same method
for person in (team_lead, team_member):
    person.occupation()
    person.has_auth()


# Polymporphism with Inheritance
# Polymorphism in python defines methods in the child class that have the same name as the methods in the parent class

class Zuitt:
    def tracks(self):
        print("We are currently offering 3 tracks(developer career, pi-shape career and short courses)")

    def num_of_hours(self):
        print("Learn web development in 360 hours")


class Developer(Zuitt):
    # overriding the parent class method
    def num_of_hours(self):
        print("Learn the basics of web dev in 240 hours")


class PiShape(Zuitt):
    # overriding the parent class method
    def num_of_hours(self):
        print("Learn the basics of app dev in 140 hours")


class ShortCourse(Zuitt):
    # overriding the parent class method
    def num_of_hours(self):
        print("Learn advance topics in web dev in 20 hours")


course_one = Developer()
course_two = PiShape()
course_three = ShortCourse()

for course in (course_one, course_two, course_three):
    course.tracks()
    course.num_of_hours()


# Fourth Pillar: Abstraction
# Abstraction is a mechanism of hiding the internal details and showing only functionalities
# Can be considered as a blueprint of a other classes.
# It allows you to create a set of methods that must be created within any child classes built from the abstract class
# Needs to import ABC and abstractmethod from abc module.
# Abstract Base Class (ABC) is a class that is used to create abstract classes


class Polygon(ABC):
    # abstract method
    def print_number_of_sides(self):
        # pass is a keyword that does nothing but is required by python
        pass


class Triangle(Polygon):
    def __init__(self):
        super().__init__()

    # overriding abstract method
    def print_number_of_sides(self):
        print(f"This polygon has 3 sides")


class Pentagon(Polygon):
    def __init__(self):
        super().__init__()

    # overriding abstract method\
    def print_number_of_sides(self):
        print(f"This polygon has 5 sides")


shape_one = Triangle()
shape_two = Pentagon()
shape_one.print_number_of_sides()
shape_two.print_number_of_sides()

for shape in (shape_one, shape_two):
    shape.print_number_of_sides()















